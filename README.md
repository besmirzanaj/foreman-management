This ansible playbook to manage a Foreman instance.

Main article here: <https://cloudalbania.com/2020-08-set-up-foreman-and-manage-it-with-ansible/>

Also posted in Foreman community website: <https://community.theforeman.org/t/set-up-foreman-and-manage-it-with-ansible/22717>